﻿using System;
using NL2SQLLib;
using NL2SQLLib.EntityFramework.Controller;
using NL2SQLLib.QueryParsing.Controller;
using NL2SQLLib.SuggestionsGeneration;

namespace NL2SQLTest
{
    internal class Program
    {
        private static Context _ctx = new Context();

        private static readonly LogManager Logger = LogManager.GetLogger();

        private static void Main(string[] args)
        {
            // Trying the schemaParser
            //SchemaParser.ReadXmlFile("schema-template.xml");
            //SchemaParser.DescribeSchema();
            //SchemaParser.AddSchemaDetailsToAutocorrectFile("mac-words.txt");
            //SchemaParser.SaveSchemaToDb();

            var parser = new QueryParser();
            var input = Console.ReadLine();

            var correctedInput = parser.AutoCorrectInput(input);
            var selection = "n";
            if (!correctedInput.Equals(input, StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("Did you mean -> " + correctedInput + "?");
                selection = Console.ReadLine();
            }
            if (selection != null)
                switch (selection.ToLower())
                {
                    case "y":
                        parser.ParseNaturalLanguageInput(correctedInput, 2, true);
                        break;
                    case "n":
                        parser.ParseNaturalLanguageInput(input, 2, false);
                        break;
                }
            input = Console.ReadLine();
            var sg = new SuggestionsGenerator(1);
            var suggest = sg.GenerateSuggestionsList(input);

            foreach (var c in suggest)

                Console.WriteLine(c);
        }
    }
}