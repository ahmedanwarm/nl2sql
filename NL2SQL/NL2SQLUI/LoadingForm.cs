﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;
using NL2SQLLib;
using NL2SQLLib.EntityFramework.Controller;
using NL2SQLLib.EntityFramework.Model;
using NL2SQLUI.Properties;

namespace NL2SQLUI
{
    public partial class LoadingForm : MetroForm
    {
        private static readonly LogManager Logger = LogManager.GetLogger();
        private Context _ctx;
        private List<UserSchema> _schemata = new List<UserSchema>();

        public LoadingForm()
        {
            InitializeComponent();
            Application.EnableVisualStyles();
            loadingProgressBar.Value = 0;
        }

        private void LoadingForm_Load(object sender, EventArgs e)
        {
            metroLabelLoadingPercentage.Text = loadingProgressBar.ProgressTotalPercent + "%";
            var connectToDatabaseThread = new Thread(BeginLoadingActions);
            connectToDatabaseThread.Start();
        }

        private void BeginLoadingActions()
        {
            try
            {
                if (ConnectToDatabase())
                    if (DiscoverSchemata())
                        Logger.Log("Loading completed", LogManager.Info);
            }
            catch (Exception ex)
            {
                Logger.Log("an error has occured, see stack trace below\n", LogManager.Error, ex);
            }
            finally
            {
                HandleThreadDone();
            }
        }

        private bool DiscoverSchemata()
        {
            _schemata = null;
            try
            {
                if (_ctx == null)
                    _ctx = new Context();
                if (_ctx.Database.Exists())
                {
                    _schemata = _ctx.Schemata.ToList();
                    if (_schemata != null && _schemata.Any())
                    {
                        Invoke(new Action(() =>
                        {
                            var count = 0;
                            while (count < 50)
                            {
                                loadingProgressBar.Value++;
                                Thread.Sleep(15);
                                count++;
                            }
                            labelStatus.Text = Resources.LoadingForm_DiscoverSchemata_Status_Got_schema_list_from_DB;
                            metroLabelLoadingPercentage.Text = loadingProgressBar.ProgressTotalPercent + "%";
                        }));
                        Logger.Log("Got schema list from database successfully\nCount: " + _schemata.Count,
                            LogManager.Info);
                        foreach (var schema in _schemata)
                            Logger.Log("Schema Name: " + schema.SchemaName + "-" + schema.SchemaAlias, LogManager.Info);
                    }
                }
                else
                {
                    throw new Exception("Connection to database could not be established");
                }
            }
            catch (Exception ex)
            {
                Logger.Log("an error has occured, see stack trace below\n", LogManager.Error, ex);
                return false;
            }
            return true;
        }

        private bool ConnectToDatabase()
        {
            try
            {
                var ctx = new Context();
                if (ctx.Database.Exists())
                {
                    var count = 0;
                    while (count < 50)
                    {
                        loadingProgressBar.Value++;
                        Thread.Sleep(15);
                        count++;
                    }
                    Logger.Log("Connected to database successfully", LogManager.Info);
                    Invoke(new Action(() =>
                    {
                        labelStatus.Text = Resources.LoadingForm_ConnectToDatabase_Status_Connected_to_DB;
                        metroLabelLoadingPercentage.Text = loadingProgressBar.ProgressTotalPercent + "%";
                    }));
                    return true;
                }
                Invoke(
                    new Action(
                        () =>
                        {
                            MetroMessageBox.Show(this, "Failed to connect to database, see logs for more details",
                                "ERROR");
                        }));
                throw new Exception("Failed to connect to database");
            }
            catch (Exception ex)
            {
                Logger.Log("An error has occured, see stack trace below\n", LogManager.Error, ex);
                return false;
            }
        }

        private void HandleThreadDone()
        {
            if (loadingProgressBar.Value == loadingProgressBar.Maximum)
            {
                Invoke(new Action(() =>
                {
                    var schemaSelector = new SchemaSelectorForm(_schemata, _ctx);
                    Hide();
                    schemaSelector.Show();
                }));
            }
            else
            {
                Logger.Log(Resources.LoadingForm_HandleThreadDone_Waiting_for_other_threads_to_complete, LogManager.Info);
                Invoke(
                    new Action(
                        () =>
                        {
                            labelStatus.Text =
                                Resources.LoadingForm_HandleThreadDone_Waiting_for_other_threads_to_complete;
                        }));
            }
        }

        private void LoadingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}