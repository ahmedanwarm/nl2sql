﻿namespace NL2SQLUI
{
    partial class AddSchemaFromXmlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openXmlSchemaDialog = new System.Windows.Forms.OpenFileDialog();
            this.textSchemaPath = new System.Windows.Forms.TextBox();
            this.btnBrowseForSchema = new MetroFramework.Controls.MetroButton();
            this.btnReadSchemaFile = new MetroFramework.Controls.MetroButton();
            this.btnValidateXml = new MetroFramework.Controls.MetroButton();
            this.labelStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textSchemaPath
            // 
            this.textSchemaPath.Enabled = false;
            this.textSchemaPath.Location = new System.Drawing.Point(24, 100);
            this.textSchemaPath.Name = "textSchemaPath";
            this.textSchemaPath.Size = new System.Drawing.Size(554, 20);
            this.textSchemaPath.TabIndex = 0;
            // 
            // btnBrowseForSchema
            // 
            this.btnBrowseForSchema.Location = new System.Drawing.Point(584, 90);
            this.btnBrowseForSchema.Name = "btnBrowseForSchema";
            this.btnBrowseForSchema.Size = new System.Drawing.Size(111, 40);
            this.btnBrowseForSchema.TabIndex = 1;
            this.btnBrowseForSchema.Text = "Browse";
            this.btnBrowseForSchema.UseSelectable = true;
            this.btnBrowseForSchema.Click += new System.EventHandler(this.btnBrowseForSchema_Click);
            // 
            // btnReadSchemaFile
            // 
            this.btnReadSchemaFile.Location = new System.Drawing.Point(373, 184);
            this.btnReadSchemaFile.Name = "btnReadSchemaFile";
            this.btnReadSchemaFile.Size = new System.Drawing.Size(254, 86);
            this.btnReadSchemaFile.TabIndex = 2;
            this.btnReadSchemaFile.Text = "Read XML File And Save";
            this.btnReadSchemaFile.UseSelectable = true;
            this.btnReadSchemaFile.Click += new System.EventHandler(this.btnReadSchemaFile_Click);
            // 
            // btnValidateXml
            // 
            this.btnValidateXml.Location = new System.Drawing.Point(94, 184);
            this.btnValidateXml.Name = "btnValidateXml";
            this.btnValidateXml.Size = new System.Drawing.Size(254, 86);
            this.btnValidateXml.TabIndex = 3;
            this.btnValidateXml.Text = "Validate XML File";
            this.btnValidateXml.UseSelectable = true;
            this.btnValidateXml.Click += new System.EventHandler(this.btnValidateXml_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(24, 290);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(74, 13);
            this.labelStatus.TabIndex = 4;
            this.labelStatus.Text = "Status: Ready";
            // 
            // AddSchemaFromXmlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 326);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.btnValidateXml);
            this.Controls.Add(this.btnReadSchemaFile);
            this.Controls.Add(this.btnBrowseForSchema);
            this.Controls.Add(this.textSchemaPath);
            this.MaximizeBox = false;
            this.Name = "AddSchemaFromXmlForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Add Schema From XML";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddSchemaFromXmlForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openXmlSchemaDialog;
        private System.Windows.Forms.TextBox textSchemaPath;
        private MetroFramework.Controls.MetroButton btnBrowseForSchema;
        private MetroFramework.Controls.MetroButton btnReadSchemaFile;
        private MetroFramework.Controls.MetroButton btnValidateXml;
        private System.Windows.Forms.Label labelStatus;
    }
}