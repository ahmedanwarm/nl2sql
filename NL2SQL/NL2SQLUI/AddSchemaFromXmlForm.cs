﻿using System;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;
using NL2SQLLib;
using NL2SQLLib.SchemaParsing;

namespace NL2SQLUI
{
    public partial class AddSchemaFromXmlForm : MetroForm
    {
        private static readonly LogManager Logger = LogManager.GetLogger();

        public AddSchemaFromXmlForm()
        {
            Application.EnableVisualStyles();
            InitializeComponent();
        }

        private void btnBrowseForSchema_Click(object sender, EventArgs e)
        {
            openXmlSchemaDialog.ShowDialog(this);
            if (openXmlSchemaDialog.FileName.EndsWith(".xml", StringComparison.InvariantCultureIgnoreCase))
            {
                textSchemaPath.Text = openXmlSchemaDialog.FileName;
            }
            else
            {
                openXmlSchemaDialog.FileName = null;
                MetroMessageBox.Show(this, "Please select an XML file", "ERROR");
            }
        }

        private void btnReadSchemaFile_Click(object sender, EventArgs e)
        {
            btnValidateXml_Click(sender, e);
            if (!string.IsNullOrEmpty(textSchemaPath.Text))
                try
                {
                    SchemaParser.ReadXmlFile(textSchemaPath.Text);
                    var addedSchema = SchemaParser.SaveSchemaToDb();
                    Hide();
                    if (MainForm.Instance != null)
                    {
                        MainForm.Instance.Show();
                        MainForm.Instance.Activate();
                    }
                    else
                    {
                        var form = new MainForm(addedSchema);
                        form.Show();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log("Could not read XML Schema, see exception details below", LogManager.Error, ex);
                }
        }

        private void AddSchemaFromXmlForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnValidateXml_Click(object sender, EventArgs e)
        {
            try
            {
                if (SchemaParser.ValidateXmlFile(textSchemaPath.Text))
                {
                    Logger.Log("XML Schema file is valid", LogManager.Debug);
                    labelStatus.Text = "Status: XML Schema is valid";
                }
                else
                {
                    Logger.Log("XML Schema file is invalid", LogManager.Debug);
                    labelStatus.Text = "Status: XML Schema file is invalid, check logs for more details";
                    textSchemaPath.Text = "";
                    openXmlSchemaDialog.FileName = "";
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Could not validate XML file", LogManager.Error, ex);
            }
        }
    }
}