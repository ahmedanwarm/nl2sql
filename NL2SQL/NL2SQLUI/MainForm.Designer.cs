﻿namespace NL2SQLUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnConvertToSQL = new MetroFramework.Controls.MetroButton();
            this.textBoxNaturalLanguage = new MetroFramework.Controls.MetroTextBox();
            this.comboBoxSuggestions = new MetroFramework.Controls.MetroComboBox();
            this.textBoxSQLQuery = new MetroFramework.Controls.MetroTextBox();
            this.btnCopySQLQuery = new MetroFramework.Controls.MetroButton();
            this.checkBoxAutocorrect = new MetroFramework.Controls.MetroCheckBox();
            this.labelOptions = new MetroFramework.Controls.MetroLabel();
            this.checkBoxSuggestions = new MetroFramework.Controls.MetroCheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Enter Natural Language Sentence";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 332);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Converted SQL Query";
            // 
            // btnConvertToSQL
            // 
            this.btnConvertToSQL.Location = new System.Drawing.Point(241, 280);
            this.btnConvertToSQL.Name = "btnConvertToSQL";
            this.btnConvertToSQL.Size = new System.Drawing.Size(213, 34);
            this.btnConvertToSQL.TabIndex = 6;
            this.btnConvertToSQL.Text = "Convert To SQL";
            this.btnConvertToSQL.UseSelectable = true;
            this.btnConvertToSQL.Click += new System.EventHandler(this.btnConvertToSQL_Click);
            // 
            // textBoxNaturalLanguage
            // 
            // 
            // 
            // 
            this.textBoxNaturalLanguage.CustomButton.Image = null;
            this.textBoxNaturalLanguage.CustomButton.Location = new System.Drawing.Point(618, 1);
            this.textBoxNaturalLanguage.CustomButton.Name = "";
            this.textBoxNaturalLanguage.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.textBoxNaturalLanguage.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.textBoxNaturalLanguage.CustomButton.TabIndex = 1;
            this.textBoxNaturalLanguage.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.textBoxNaturalLanguage.CustomButton.UseSelectable = true;
            this.textBoxNaturalLanguage.CustomButton.Visible = false;
            this.textBoxNaturalLanguage.Lines = new string[0];
            this.textBoxNaturalLanguage.Location = new System.Drawing.Point(23, 103);
            this.textBoxNaturalLanguage.MaxLength = 32767;
            this.textBoxNaturalLanguage.Name = "textBoxNaturalLanguage";
            this.textBoxNaturalLanguage.PasswordChar = '\0';
            this.textBoxNaturalLanguage.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textBoxNaturalLanguage.SelectedText = "";
            this.textBoxNaturalLanguage.SelectionLength = 0;
            this.textBoxNaturalLanguage.SelectionStart = 0;
            this.textBoxNaturalLanguage.Size = new System.Drawing.Size(640, 23);
            this.textBoxNaturalLanguage.TabIndex = 7;
            this.textBoxNaturalLanguage.UseSelectable = true;
            this.textBoxNaturalLanguage.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.textBoxNaturalLanguage.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.textBoxNaturalLanguage.TextChanged += new System.EventHandler(this.textBoxNaturalLanguage_TextChanged);
            this.textBoxNaturalLanguage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxNaturalLanguage_KeyDown);
            // 
            // comboBoxSuggestions
            // 
            this.comboBoxSuggestions.FormattingEnabled = true;
            this.comboBoxSuggestions.ItemHeight = 23;
            this.comboBoxSuggestions.Location = new System.Drawing.Point(23, 103);
            this.comboBoxSuggestions.Name = "comboBoxSuggestions";
            this.comboBoxSuggestions.Size = new System.Drawing.Size(640, 29);
            this.comboBoxSuggestions.TabIndex = 8;
            this.comboBoxSuggestions.UseSelectable = true;
            this.comboBoxSuggestions.Visible = false;
            this.comboBoxSuggestions.SelectedIndexChanged += new System.EventHandler(this.comboBoxSuggestions_SelectedIndexChanged);
            // 
            // textBoxSQLQuery
            // 
            // 
            // 
            // 
            this.textBoxSQLQuery.CustomButton.Image = null;
            this.textBoxSQLQuery.CustomButton.Location = new System.Drawing.Point(477, 1);
            this.textBoxSQLQuery.CustomButton.Name = "";
            this.textBoxSQLQuery.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.textBoxSQLQuery.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.textBoxSQLQuery.CustomButton.TabIndex = 1;
            this.textBoxSQLQuery.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.textBoxSQLQuery.CustomButton.UseSelectable = true;
            this.textBoxSQLQuery.CustomButton.Visible = false;
            this.textBoxSQLQuery.Lines = new string[0];
            this.textBoxSQLQuery.Location = new System.Drawing.Point(23, 348);
            this.textBoxSQLQuery.MaxLength = 32767;
            this.textBoxSQLQuery.Name = "textBoxSQLQuery";
            this.textBoxSQLQuery.PasswordChar = '\0';
            this.textBoxSQLQuery.ReadOnly = true;
            this.textBoxSQLQuery.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textBoxSQLQuery.SelectedText = "";
            this.textBoxSQLQuery.SelectionLength = 0;
            this.textBoxSQLQuery.SelectionStart = 0;
            this.textBoxSQLQuery.Size = new System.Drawing.Size(499, 23);
            this.textBoxSQLQuery.TabIndex = 9;
            this.textBoxSQLQuery.UseSelectable = true;
            this.textBoxSQLQuery.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.textBoxSQLQuery.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnCopySQLQuery
            // 
            this.btnCopySQLQuery.Location = new System.Drawing.Point(529, 347);
            this.btnCopySQLQuery.Name = "btnCopySQLQuery";
            this.btnCopySQLQuery.Size = new System.Drawing.Size(134, 23);
            this.btnCopySQLQuery.TabIndex = 10;
            this.btnCopySQLQuery.Text = "Copy SQL Query";
            this.btnCopySQLQuery.UseSelectable = true;
            this.btnCopySQLQuery.Click += new System.EventHandler(this.btnCopySQLQuery_Click);
            // 
            // checkBoxAutocorrect
            // 
            this.checkBoxAutocorrect.AutoSize = true;
            this.checkBoxAutocorrect.Checked = true;
            this.checkBoxAutocorrect.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAutocorrect.Location = new System.Drawing.Point(23, 181);
            this.checkBoxAutocorrect.Name = "checkBoxAutocorrect";
            this.checkBoxAutocorrect.Size = new System.Drawing.Size(103, 15);
            this.checkBoxAutocorrect.Style = MetroFramework.MetroColorStyle.Black;
            this.checkBoxAutocorrect.TabIndex = 11;
            this.checkBoxAutocorrect.Text = "Autocorrection";
            this.checkBoxAutocorrect.UseSelectable = true;
            // 
            // labelOptions
            // 
            this.labelOptions.AutoSize = true;
            this.labelOptions.Location = new System.Drawing.Point(23, 159);
            this.labelOptions.Name = "labelOptions";
            this.labelOptions.Size = new System.Drawing.Size(55, 19);
            this.labelOptions.TabIndex = 12;
            this.labelOptions.Text = "Options";
            // 
            // checkBoxSuggestions
            // 
            this.checkBoxSuggestions.AutoSize = true;
            this.checkBoxSuggestions.Checked = true;
            this.checkBoxSuggestions.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSuggestions.Location = new System.Drawing.Point(23, 202);
            this.checkBoxSuggestions.Name = "checkBoxSuggestions";
            this.checkBoxSuggestions.Size = new System.Drawing.Size(87, 15);
            this.checkBoxSuggestions.Style = MetroFramework.MetroColorStyle.Black;
            this.checkBoxSuggestions.TabIndex = 13;
            this.checkBoxSuggestions.Text = "Suggestions";
            this.checkBoxSuggestions.UseSelectable = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 394);
            this.Controls.Add(this.checkBoxSuggestions);
            this.Controls.Add(this.labelOptions);
            this.Controls.Add(this.checkBoxAutocorrect);
            this.Controls.Add(this.btnCopySQLQuery);
            this.Controls.Add(this.textBoxSQLQuery);
            this.Controls.Add(this.textBoxNaturalLanguage);
            this.Controls.Add(this.btnConvertToSQL);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxSuggestions);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Convert Natural Language to SQL";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroButton btnConvertToSQL;
        private MetroFramework.Controls.MetroTextBox textBoxNaturalLanguage;
        private MetroFramework.Controls.MetroComboBox comboBoxSuggestions;
        private MetroFramework.Controls.MetroTextBox textBoxSQLQuery;
        private MetroFramework.Controls.MetroButton btnCopySQLQuery;
        private MetroFramework.Controls.MetroCheckBox checkBoxAutocorrect;
        private MetroFramework.Controls.MetroLabel labelOptions;
        private MetroFramework.Controls.MetroCheckBox checkBoxSuggestions;
    }
}