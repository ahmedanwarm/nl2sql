﻿namespace NL2SQLUI
{
    partial class SchemaSelectorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tileAddDatabaseSchemaFromXML = new MetroFramework.Controls.MetroTile();
            this.listBoxSchemata = new System.Windows.Forms.ListBox();
            this.btnSubmit = new MetroFramework.Controls.MetroButton();
            this.labelAddSchemaFromXML = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // tileAddDatabaseSchemaFromXML
            // 
            this.tileAddDatabaseSchemaFromXML.ActiveControl = null;
            this.tileAddDatabaseSchemaFromXML.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tileAddDatabaseSchemaFromXML.Location = new System.Drawing.Point(478, 63);
            this.tileAddDatabaseSchemaFromXML.Name = "tileAddDatabaseSchemaFromXML";
            this.tileAddDatabaseSchemaFromXML.Size = new System.Drawing.Size(224, 355);
            this.tileAddDatabaseSchemaFromXML.Style = MetroFramework.MetroColorStyle.White;
            this.tileAddDatabaseSchemaFromXML.TabIndex = 0;
            this.tileAddDatabaseSchemaFromXML.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tileAddDatabaseSchemaFromXML.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tileAddDatabaseSchemaFromXML.TileImage = global::NL2SQLUI.Properties.Resources.ImgAddSchema;
            this.tileAddDatabaseSchemaFromXML.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tileAddDatabaseSchemaFromXML.TileTextFontSize = MetroFramework.MetroTileTextSize.Small;
            this.tileAddDatabaseSchemaFromXML.UseSelectable = true;
            this.tileAddDatabaseSchemaFromXML.UseStyleColors = true;
            this.tileAddDatabaseSchemaFromXML.UseTileImage = true;
            this.tileAddDatabaseSchemaFromXML.Click += new System.EventHandler(this.tileAddDatabaseSchemaFromXML_Click);
            // 
            // listBoxSchemata
            // 
            this.listBoxSchemata.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listBoxSchemata.FormattingEnabled = true;
            this.listBoxSchemata.Location = new System.Drawing.Point(23, 63);
            this.listBoxSchemata.Name = "listBoxSchemata";
            this.listBoxSchemata.Size = new System.Drawing.Size(373, 355);
            this.listBoxSchemata.TabIndex = 1;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSubmit.Location = new System.Drawing.Point(23, 425);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(373, 41);
            this.btnSubmit.TabIndex = 2;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseSelectable = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // labelAddSchemaFromXML
            // 
            this.labelAddSchemaFromXML.AutoSize = true;
            this.labelAddSchemaFromXML.Location = new System.Drawing.Point(503, 421);
            this.labelAddSchemaFromXML.Name = "labelAddSchemaFromXML";
            this.labelAddSchemaFromXML.Size = new System.Drawing.Size(177, 19);
            this.labelAddSchemaFromXML.Style = MetroFramework.MetroColorStyle.Silver;
            this.labelAddSchemaFromXML.TabIndex = 3;
            this.labelAddSchemaFromXML.Text = "Add New Schema from XML";
            this.labelAddSchemaFromXML.Theme = MetroFramework.MetroThemeStyle.Light;
            this.labelAddSchemaFromXML.UseCustomBackColor = true;
            this.labelAddSchemaFromXML.UseStyleColors = true;
            // 
            // SchemaSelectorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackImage = global::NL2SQLUI.Properties.Resources.ImgAddSchema;
            this.ClientSize = new System.Drawing.Size(745, 490);
            this.Controls.Add(this.labelAddSchemaFromXML);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.listBoxSchemata);
            this.Controls.Add(this.tileAddDatabaseSchemaFromXML);
            this.MaximizeBox = false;
            this.Name = "SchemaSelectorForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Schema Selector";
            this.Theme = MetroFramework.MetroThemeStyle.Default;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SchemaSelector_FormClosing);
            this.Load += new System.EventHandler(this.SchemaSelector_Load);
            this.Shown += new System.EventHandler(this.SchemaSelectorForm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTile tileAddDatabaseSchemaFromXML;
        private System.Windows.Forms.ListBox listBoxSchemata;
        private MetroFramework.Controls.MetroButton btnSubmit;
        private MetroFramework.Controls.MetroLabel labelAddSchemaFromXML;
    }
}