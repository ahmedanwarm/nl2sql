﻿namespace NL2SQLUI
{
    partial class LoadingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadingForm));
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.loadingProgressBar = new MetroFramework.Controls.MetroProgressBar();
            this.metroLabelLoading = new MetroFramework.Controls.MetroLabel();
            this.metroLabelLoadingPercentage = new MetroFramework.Controls.MetroLabel();
            this.labelStatus = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBoxLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.BackgroundImage")));
            this.pictureBoxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBoxLogo.Location = new System.Drawing.Point(24, 64);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(632, 259);
            this.pictureBoxLogo.TabIndex = 0;
            this.pictureBoxLogo.TabStop = false;
            // 
            // loadingProgressBar
            // 
            this.loadingProgressBar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loadingProgressBar.Location = new System.Drawing.Point(23, 329);
            this.loadingProgressBar.Name = "loadingProgressBar";
            this.loadingProgressBar.Size = new System.Drawing.Size(633, 34);
            this.loadingProgressBar.TabIndex = 1;
            // 
            // metroLabelLoading
            // 
            this.metroLabelLoading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.metroLabelLoading.AutoSize = true;
            this.metroLabelLoading.Location = new System.Drawing.Point(567, 385);
            this.metroLabelLoading.Name = "metroLabelLoading";
            this.metroLabelLoading.Size = new System.Drawing.Size(63, 19);
            this.metroLabelLoading.TabIndex = 2;
            this.metroLabelLoading.Text = "Loading: ";
            // 
            // metroLabelLoadingPercentage
            // 
            this.metroLabelLoadingPercentage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.metroLabelLoadingPercentage.AutoSize = true;
            this.metroLabelLoadingPercentage.Location = new System.Drawing.Point(618, 385);
            this.metroLabelLoadingPercentage.Name = "metroLabelLoadingPercentage";
            this.metroLabelLoadingPercentage.Size = new System.Drawing.Size(27, 19);
            this.metroLabelLoadingPercentage.TabIndex = 3;
            this.metroLabelLoadingPercentage.Text = "0%";
            // 
            // labelStatus
            // 
            this.labelStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(24, 385);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(107, 19);
            this.labelStatus.TabIndex = 4;
            this.labelStatus.Text = "Status: Initializing";
            // 
            // LoadingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 424);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.metroLabelLoadingPercentage);
            this.Controls.Add(this.metroLabelLoading);
            this.Controls.Add(this.loadingProgressBar);
            this.Controls.Add(this.pictureBoxLogo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoadingForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Natural Language to SQL";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoadingForm_FormClosing);
            this.Load += new System.EventHandler(this.LoadingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private MetroFramework.Controls.MetroLabel metroLabelLoading;
        private MetroFramework.Controls.MetroLabel metroLabelLoadingPercentage;
        public MetroFramework.Controls.MetroProgressBar loadingProgressBar;
        private MetroFramework.Controls.MetroLabel labelStatus;
    }
}

