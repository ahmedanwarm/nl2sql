﻿using System;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;
using NL2SQLLib;
using NL2SQLLib.EntityFramework.Model;
using NL2SQLLib.QueryParsing.Controller;
using NL2SQLLib.SuggestionsGeneration;

namespace NL2SQLUI
{
    public partial class MainForm : MetroForm
    {
        private static SuggestionsGenerator _generator;
        public static MainForm Instance;
        private static readonly LogManager Logger = LogManager.GetLogger();
        private readonly UserSchema _schema;

        public MainForm(UserSchema schema)
        {
            Application.EnableVisualStyles();
            InitializeComponent();
            _schema = schema;
            _generator = new SuggestionsGenerator(_schema.UserSchemaId);
            Logger.Log("Target Schema\n\tName: " + _schema.SchemaAlias + "\n\tID: " + _schema.UserSchemaId,
                LogManager.Info);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Application.EnableVisualStyles();
            Instance = this;
        }

        private void btnConvertToSQL_Click(object sender, EventArgs e)
        {
            comboBoxSuggestions.Items.Clear();
            comboBoxSuggestions.DroppedDown = false;
            comboBoxSuggestions.Visible = false;
            var input = textBoxNaturalLanguage.Text;
            var parser = new QueryParser();
            if (checkBoxAutocorrect.Checked)
            {
                var correctedInput = parser.AutoCorrectInput(input);
                if (!correctedInput.Equals(input, StringComparison.InvariantCultureIgnoreCase))
                {
                    var result = MetroMessageBox.Show(this, "Did you mean -> " + correctedInput + "?",
                        "Spelling mistake detected",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        input = correctedInput;
                        textBoxNaturalLanguage.Text = input;
                        textBoxSQLQuery.Text = parser.ParseNaturalLanguageInput(correctedInput, _schema.UserSchemaId,
                            true);
                        return;
                    }
                }
            }
            textBoxSQLQuery.Text = parser.ParseNaturalLanguageInput(input, _schema.UserSchemaId, false);
        }

        private void textBoxNaturalLanguage_TextChanged(object sender, EventArgs e)
        {
            if(comboBoxSuggestions.DroppedDown)
                comboBoxSuggestions.DroppedDown = false;
            if(comboBoxSuggestions.Visible)
                comboBoxSuggestions.Hide();
            if (!checkBoxSuggestions.Checked || !textBoxNaturalLanguage.Text.EndsWith(" "))
                return;
            var suggestions = _generator.GenerateSuggestionsList(textBoxNaturalLanguage.Text.TrimEnd(' '));
            if (suggestions.Count == 0)
                return;
            comboBoxSuggestions.Items.Clear();
            foreach (var t in suggestions)
                comboBoxSuggestions.Items.Add(t.ToLower().TrimEnd(' '));

            comboBoxSuggestions.Visible = true;
            comboBoxSuggestions.DroppedDown = true;
        }

        private void comboBoxSuggestions_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxSuggestions.DroppedDown = false;
            var suggestion = comboBoxSuggestions.SelectedItem.ToString();
            textBoxNaturalLanguage.Text += suggestion + " ";
            comboBoxSuggestions.Items.Clear();
            comboBoxSuggestions.Visible = false;
        }

        private void btnCopySQLQuery_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBoxSQLQuery.Text);
            textBoxSQLQuery.SelectAll();
            textBoxSQLQuery.Focus();
        }

        private void textBoxNaturalLanguage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.Equals(Keys.Enter))
            {
                btnConvertToSQL.PerformClick();
            }
        }
    }
}