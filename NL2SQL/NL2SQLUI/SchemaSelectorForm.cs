﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Forms;
using NL2SQLLib;
using NL2SQLLib.EntityFramework.Controller;
using NL2SQLLib.EntityFramework.Model;

namespace NL2SQLUI
{
    public partial class SchemaSelectorForm : MetroForm
    {
        public static SchemaSelectorForm Instance;
        private static readonly LogManager Logger = LogManager.GetLogger();
        private readonly Context _ctx;
        private List<UserSchema> _schemata;
        private List<int> _schemataIds;

        public SchemaSelectorForm(List<UserSchema> schemata, Context ctx)
        {
            InitializeComponent();
            Application.EnableVisualStyles();
            if (schemata.Any())
            {
                _schemataIds = new List<int>();
                _schemata = schemata;
                foreach (var schema in _schemata)
                    if (schema != null)
                    {
                        listBoxSchemata.Items.Add(schema.UserSchemaId + ". " + schema.SchemaAlias);
                        _schemataIds.Add(schema.UserSchemaId);
                    }
            }
            if (ctx != null)
                _ctx = ctx;
        }

        private void SchemaSelector_Load(object sender, EventArgs e)
        {
            Instance = this;
        }

        private void SchemaSelector_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var selectedSchemaId = _schemataIds[listBoxSchemata.SelectedIndex];
            var selectedSchema = _ctx.GetSchemaById(selectedSchemaId);
            Hide();
            if (MainForm.Instance != null)
            {
                MainForm.Instance.Show();
                MainForm.Instance.Activate();
            }
            else
            {
                var form = new MainForm(selectedSchema);
                form.Show();
            }
        }

        private void tileAddDatabaseSchemaFromXML_Click(object sender, EventArgs e)
        {
            var addSchemaFromXmlForm = new AddSchemaFromXmlForm();
            Hide();
            addSchemaFromXmlForm.Show();
        }

        private void SchemaSelectorForm_Shown(object sender, EventArgs e)
        {
            Logger.Log("Reloading schema list", LogManager.Debug);
            _schemata = _ctx.Schemata.ToList();
            listBoxSchemata.Items.Clear();
            _schemataIds = new List<int>();
            foreach (var schema in _schemata)
                if (schema != null)
                {
                    listBoxSchemata.Items.Add(schema.UserSchemaId + ". " + schema.SchemaAlias);
                    _schemataIds.Add(schema.UserSchemaId);
                }
        }
    }
}