﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NL2SQLLib.EntityFramework.Controller;
using NL2SQLLib.EntityFramework.Model;
using NL2SQLLib.QueryParsing.Model;
using NL2SQLLib.SchemaParsing;
using SpellingCorrector;

namespace NL2SQLLib.QueryParsing.Controller
{
    public class QueryParser
    {
        private static readonly LogManager Logger = LogManager.GetLogger();
        private static readonly List<string> IgnoreWordsList = new List<string>();
        private readonly Context _ctx = new Context();
        private bool _checkForColumns = true;
        private string _cmdSynonymNameInInput = "";
        private string _conditionCmdSynonymName;
        private List<ConditionOperator> _conditionOperators;
        private string _inputBeforeCondition, _inputAfterCondition;
        private bool _inputContainsCondition;
        private int _schemaId;
        private string[] _statementParts;
        private string _userInput;
        public ParsedInput ParsedInput;

        public QueryParser()
        {
            ParsedInput = new ParsedInput();
            IgnoreWordsList.Add("the");
            IgnoreWordsList.Add("all");
            IgnoreWordsList.Add("a");
            IgnoreWordsList.Add("an");
            IgnoreWordsList.Add("me");
        }

        public string RemoveIgnoreWords(string input)
        {
            var inputParts = input.Split(' ').ToList();
            foreach (var word in IgnoreWordsList)
                while (inputParts.Contains(word))
                    inputParts.Remove(word);
            input = inputParts.Aggregate("", (current, t) => current + t + " ");
            Logger.Log("Removed ignore words from input: " + input, LogManager.Info);
            return input.TrimEnd(' ');
        }

        public string ParseNaturalLanguageInput(string input, int schemaId, bool doCorrect)
        {
            //List<ConditionOperator> ops = _ctx.ConditionOperators.ToList();
            //foreach (var conditionOperator in ops)
            //{
            //    Logger.Log(conditionOperator.ConditionOperatorName, LogManager.Debug);
            //}
            string sqlStatement = null;
            try
            {
                Logger.Log("Attempting to parse input: " + input, LogManager.Info);
                input = input.ToLower();
                input = RemoveIgnoreWords(input);
                _userInput = input;
                _schemaId = schemaId;
                if (doCorrect)
                {
                    var correctedInput = AutoCorrectInput(input);
                    if (!correctedInput.Equals(input, StringComparison.InvariantCultureIgnoreCase))
                    {
                        Logger.Log("Autocorrected input from: \n\t" + input + "\nTo:\n\t" + correctedInput,
                            LogManager.Debug);
                        input = correctedInput;
                    }
                }
                _statementParts = null;
                var conditionCmdId = _ctx.Commands.ToList().Find(x => x.CmdName == "WHERE").CmdId;
                _conditionCmdSynonymName = "";
                foreach (var conditionCmdSynonym in _ctx.CommandSynonyms)
                    if (conditionCmdSynonym.CmdId == conditionCmdId)
                        if (input.Contains(conditionCmdSynonym.Name))
                        {
                            _conditionCmdSynonymName = conditionCmdSynonym.Name;
                            _inputContainsCondition = true;
                            _statementParts = input.Split(new[] {" " + _conditionCmdSynonymName + " "},
                                StringSplitOptions.None);
                            _inputBeforeCondition = _statementParts[0];
                            _inputAfterCondition = _statementParts[1];
                            Logger.Log("Statement Before Condition: " + _inputBeforeCondition, LogManager.Debug);
                            Logger.Log("Statement After Condition: " + _inputAfterCondition, LogManager.Debug);
                        }
                if (!_inputContainsCondition)
                    _inputBeforeCondition = input;
                if (DetectStatementType(_inputBeforeCondition))
                    Logger.Log("Statement Type: " + ParsedInput.StatementType.CmdName, LogManager.Debug);
                if (
                    DetectTargetTable(
                        input.Substring(
                            input.IndexOf(_cmdSynonymNameInInput, StringComparison.InvariantCultureIgnoreCase) +
                            _cmdSynonymNameInInput.Length)))
                    Logger.Log("Target Table: " + ParsedInput.Table.TableName, LogManager.Debug);
                if (_checkForColumns)
                    if (
                        DetectTargetColumns(
                            _inputBeforeCondition.Substring(
                                input.IndexOf(_cmdSynonymNameInInput, StringComparison.InvariantCultureIgnoreCase) +
                                _cmdSynonymNameInInput.Length)))
                    {
                        var columnInfo = "Target Columns\n";
                        foreach (var column in ParsedInput.Columns)
                            columnInfo += column.UserColumnId + ". " + column.ColumnName + '\n';
                        Logger.Log(columnInfo, LogManager.Debug);
                    }
                if (!string.IsNullOrEmpty(_inputAfterCondition))
                {
                    _conditionOperators = _ctx.ConditionOperators.ToList();
                    if (DetectConditions(_inputAfterCondition))
                        if (ParsedInput.Conditions != null && ParsedInput.Conditions.Count != 0)
                        {
                            var log = "";
                            foreach (var condition in ParsedInput.Conditions)
                                log += condition.Column.ColumnAlias + ' ' +
                                       condition.ConditionOperator.ConditionOperatorName +
                                       condition.Value;
                            Logger.Log("Detected " + ParsedInput.Conditions.Count + "Condition(s)\n" + log,
                                LogManager.Info);
                        }
                }
                QueryGenerator queryGenerator = null;
                switch (ParsedInput.StatementType.CmdName.ToUpper())
                {
                    case "SELECT":
                        queryGenerator = new SelectStatementGenerator();
                        break;
                    case "UPDATE":
                        break;
                    case "DELETE":
                        break;
                    case "INSERT":
                        break;
                }
                if (queryGenerator != null)
                    sqlStatement = queryGenerator.GenerateQueryFromParsedInput(ParsedInput);
            }
            catch (Exception ex)
            {
                Logger.Log("An excention has occured, see details below", LogManager.Error, ex);
            }
            return sqlStatement;
        }

        private bool CheckIfPreviousWordIsFromSynonym(IReadOnlyList<string> targetPartOfInputParts,
            int tableNameIndexInList)
        {
            if (tableNameIndexInList > 0)
            {
                var cmdFromSynonyms = _ctx.GetSynonymsByCmdName("FROM");
                foreach (var fromSynonym in cmdFromSynonyms)
                    if (fromSynonym.Name.Equals(targetPartOfInputParts[tableNameIndexInList - 1],
                        StringComparison.InvariantCultureIgnoreCase))
                        return true;
            }
            return false;
        }

        private bool DetectTargetTable(string targetPartOfInput)
        {
            targetPartOfInput = targetPartOfInput.TrimStart(' ').ToLower();
            var schema = _ctx.Schemata.Find(_schemaId);
            if (schema != null)
            {
                targetPartOfInput = targetPartOfInput.ToLower();
                var targetPartOfInputParts = targetPartOfInput.Split(' ').ToList();
                foreach (var table in schema.Tables)
                    if (targetPartOfInput.Contains(table.TableName.ToLower())
                        || targetPartOfInput.Contains(table.TableAlias.ToLower()))
                    {
                        for (var i = 0; i < targetPartOfInputParts.Count; i++)
                            if (targetPartOfInputParts[i].EndsWith("\'"))
                                targetPartOfInputParts[i] = targetPartOfInputParts[i].TrimEnd('\'');
                        var tableNameIndexInList = targetPartOfInputParts.IndexOf(table.TableName.ToLower());
                        if (tableNameIndexInList == -1)
                            tableNameIndexInList =
                                targetPartOfInputParts.IndexOf(
                                    SchemaParser.GetPlural(table.TableName).TrimEnd('\n').TrimEnd('\r').ToLower());
                        if (tableNameIndexInList == -1)
                            tableNameIndexInList = targetPartOfInputParts.IndexOf(table.TableAlias.ToLower());
                        if (tableNameIndexInList == -1)
                            tableNameIndexInList =
                                targetPartOfInputParts.IndexOf(
                                    SchemaParser.GetPlural(table.TableAlias).TrimEnd('\n').TrimEnd('\r').ToLower());
                        targetPartOfInputParts = targetPartOfInput.Split(' ').ToList();
                        if (tableNameIndexInList != -1)
                        {
                            if (tableNameIndexInList == 0)
                            {
                                if (targetPartOfInputParts.Count == tableNameIndexInList + 1 ||
                                    targetPartOfInputParts[tableNameIndexInList + 1].Equals(_conditionCmdSynonymName,
                                        StringComparison.InvariantCultureIgnoreCase))
                                {
                                    ParsedInput.Table = table;
                                    ParsedInput.Columns = null;
                                    _checkForColumns = false;
                                    return true;
                                }
                                if (
                                    _userInput.Contains(targetPartOfInputParts[tableNameIndexInList].TrimEnd('\'') +
                                                        '\''))
                                {
                                    ParsedInput.Table = table;
                                    return true;
                                }
                            }
                            else if (CheckIfPreviousWordIsFromSynonym(targetPartOfInputParts, tableNameIndexInList) &&
                                     (targetPartOfInputParts.Count == tableNameIndexInList + 1 ||
                                      targetPartOfInputParts[tableNameIndexInList + 1].Equals(_conditionCmdSynonymName,
                                          StringComparison.InvariantCultureIgnoreCase)))
                            {
                                ParsedInput.Table = table;
                                return true;
                            }
                            foreach (var column in table.Columns)
                                if (targetPartOfInputParts.Count > tableNameIndexInList + 1)
                                {
                                    if (
                                        column.ColumnName.ToLower()
                                            .Equals(targetPartOfInputParts[tableNameIndexInList + 1],
                                                StringComparison.InvariantCultureIgnoreCase) ||
                                        column.ColumnAlias.ToLower()
                                            .Equals(targetPartOfInputParts[tableNameIndexInList + 1],
                                                StringComparison.InvariantCultureIgnoreCase) ||
                                        SchemaParser.GetPlural(column.ColumnName)
                                            .TrimEnd('\n')
                                            .TrimEnd('\r')
                                            .Equals(targetPartOfInputParts[tableNameIndexInList + 1],
                                                StringComparison.InvariantCultureIgnoreCase) ||
                                        SchemaParser.GetPlural(column.ColumnAlias)
                                            .TrimEnd('\n')
                                            .TrimEnd('\r')
                                            .Equals(targetPartOfInputParts[tableNameIndexInList + 1],
                                                StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        ParsedInput.Table = table;
                                        return true;
                                    }
                                    if (tableNameIndexInList >= 2)
                                        if (
                                            (column.ColumnName.ToLower()
                                                 .Equals(targetPartOfInputParts[tableNameIndexInList - 2],
                                                     StringComparison.InvariantCultureIgnoreCase) ||
                                             column.ColumnAlias.ToLower()
                                                 .Equals(targetPartOfInputParts[tableNameIndexInList - 2],
                                                     StringComparison.InvariantCultureIgnoreCase) ||
                                             SchemaParser.GetPlural(column.ColumnName)
                                                 .TrimEnd('\n')
                                                 .TrimEnd('\r')
                                                 .Equals(targetPartOfInputParts[tableNameIndexInList - 2],
                                                     StringComparison.InvariantCultureIgnoreCase) ||
                                             SchemaParser.GetPlural(column.ColumnAlias)
                                                 .TrimEnd('\n')
                                                 .TrimEnd('\r')
                                                 .Equals(targetPartOfInputParts[tableNameIndexInList - 2],
                                                     StringComparison.InvariantCultureIgnoreCase)) &&
                                            CheckIfPreviousWordIsFromSynonym(targetPartOfInputParts,
                                                tableNameIndexInList))
                                        {
                                            ParsedInput.Table = table;
                                            return true;
                                        }
                                }
                        }
                    }
                if (ParsedInput.Table == null)
                    Logger.Log("No target table name or alias specified in user input", LogManager.Error);
            }
            else
            {
                Logger.Log("Not able to fetch schema using ID " + _schemaId, LogManager.Error);
            }
            return false;
        }

        private bool DetectStatementType(string targetPartOfInput)
        {
            var inputParts = targetPartOfInput.Split(' ');
            CmdSynonym cmdSynonym = null;
            foreach (var c in _ctx.CommandSynonyms)
                if (c.Name.Equals(inputParts[0], StringComparison.InvariantCultureIgnoreCase))
                {
                    cmdSynonym = c;
                    break;
                }
            if (cmdSynonym != null)
            {
                var cmdId = cmdSynonym.CmdId;
                var cmd = _ctx.Commands.Find(cmdId);
                if (cmd != null)
                {
                    ParsedInput.StatementType = cmd;
                    _cmdSynonymNameInInput = inputParts[0];
                    return true;
                }
            }
            else
            {
                Cmd cmdSelect = null;
                foreach (var c in _ctx.Commands)
                    if (c.CmdName.Equals("SELECT", StringComparison.InvariantCultureIgnoreCase))
                        cmdSelect = c;
                ParsedInput.StatementType = cmdSelect;
                return true;
            }
            return false;
        }

        private bool DetectTargetColumns(string targetPartOfInput)
        {
            targetPartOfInput = targetPartOfInput.ToLower();
            try
            {
                ParsedInput.Columns = new List<UserColumn>();
                var columnsInTargetTable = ParsedInput.Table.Columns.ToList();
                foreach (var column in columnsInTargetTable)
                    if (targetPartOfInput.Contains(column.ColumnName.ToLower()) ||
                        targetPartOfInput.Contains(column.ColumnAlias.ToLower()))
                        ParsedInput.Columns.Add(column);
                if (ParsedInput.Columns.Any())
                {
                    Logger.Log("Detected " + ParsedInput.Columns.Count + " target columns in user's input",
                        LogManager.Info);
                    return true;
                }
                Logger.Log("Could not detect any target columns", LogManager.Warning);
                return false;
            }
            catch (Exception ex)
            {
                Logger.Log("Could not determine target columns in the input", LogManager.Error, ex);
                return false;
            }
        }

        public string AutoCorrectInput(string input)
        {
            var dictionary = File.ReadAllText("mac-words.txt");
            var autoCorrector = new Spelling(dictionary);

            var partOfSpeechInfo = PartOfSpeechTagger.DetectPos(input);
            Logger.Log("Part of speech info for input: " + partOfSpeechInfo, LogManager.Debug);
            var partOfSpeechInfoParts = partOfSpeechInfo.Split(' ');
            foreach (var wordAndPos in partOfSpeechInfoParts)
            {
                var wordParts = wordAndPos.Split('/');
                var word = wordParts[0];

                if (word != "")
                {
                    var pos = wordParts[1];
                    if (pos != PartOfSpeechTagger.NounPlural &&
                        pos != PartOfSpeechTagger.NounSingularOrMass &&
                        pos != PartOfSpeechTagger.ProperNounPlural &&
                        pos != PartOfSpeechTagger.ProperNounSingular &&
                        pos != PartOfSpeechTagger.CardinalNumber)
                    {
                        var correctedWord = autoCorrector.Correct(word);
                        if (!correctedWord.Equals(word, StringComparison.InvariantCultureIgnoreCase))
                        {
                            Logger.Log("Autocorrected word from: " + word + "\tTo:" + correctedWord, LogManager.Debug);
                            input = input.Replace(word, correctedWord);
                        }
                    }
                }
            }
            return input;
        }

        private bool DetectConditions(string targetPartOfInput)
        {
            ParsedInput.Conditions = new List<Condition>();
            try
            {
                foreach (var tableColumn in ParsedInput.Table.Columns)
                {
                    var columnNameIndexInList = -1;
                    var columnNameInInput = "";
                    if (targetPartOfInput.Contains(tableColumn.ColumnAlias.ToLower()))
                    {
                        columnNameIndexInList = targetPartOfInput.IndexOf(tableColumn.ColumnAlias,
                            StringComparison.InvariantCultureIgnoreCase);
                        columnNameInInput = tableColumn.ColumnAlias.ToLower();
                    }
                    else if (targetPartOfInput.Contains(tableColumn.ColumnName.ToLower()))
                    {
                        columnNameIndexInList = targetPartOfInput.IndexOf(tableColumn.ColumnName,
                            StringComparison.InvariantCultureIgnoreCase);
                        columnNameInInput = tableColumn.ColumnName.ToLower();
                    }
                    else if (
                        targetPartOfInput.Contains(
                            SchemaParser.GetPlural(tableColumn.ColumnName.ToLower()).TrimEnd('\n').TrimEnd('\r')))
                    {
                        columnNameIndexInList =
                            targetPartOfInput.IndexOf(
                                SchemaParser.GetPlural(tableColumn.ColumnName).TrimEnd('\n').TrimEnd('\r'),
                                StringComparison.InvariantCultureIgnoreCase);
                        columnNameInInput =
                            SchemaParser.GetPlural(tableColumn.ColumnName.ToLower()).TrimEnd('\n').TrimEnd('\r');
                    }
                    else if (
                        targetPartOfInput.Contains(
                            SchemaParser.GetPlural(tableColumn.ColumnAlias.ToLower()).TrimEnd('\n').TrimEnd('\r')))
                    {
                        columnNameIndexInList =
                            targetPartOfInput.IndexOf(
                                SchemaParser.GetPlural(tableColumn.ColumnAlias).TrimEnd('\n').TrimEnd('\r'),
                                StringComparison.InvariantCultureIgnoreCase);
                        columnNameInInput = SchemaParser.GetPlural(tableColumn.ColumnAlias).TrimEnd('\n').TrimEnd('\r');
                    }

                    var lastLength = -1;
                    ConditionOperator conditionOperator = null;
                    var inputAfterColumnName =
                        targetPartOfInput.Substring(columnNameIndexInList + columnNameInInput.Length + 1);
                    foreach (var conditionOperatorAlias in _ctx.ConditionOperatorAliases)
                        if (inputAfterColumnName.StartsWith(conditionOperatorAlias.ConditionOperatorAliasName,
                            StringComparison.InvariantCultureIgnoreCase))
                            if (lastLength < conditionOperatorAlias.ConditionOperatorAliasName.Split(' ').Length)
                            {
                                lastLength =
                                    conditionOperatorAlias.ConditionOperatorAliasName.Split(new[] {' '},
                                        StringSplitOptions.RemoveEmptyEntries).Length;
                                foreach (var ctxConditionOperator in _conditionOperators)
                                    if (ctxConditionOperator.ConditionOperatorId ==
                                        conditionOperatorAlias.ParentConditionOperatorId)
                                    {
                                        conditionOperator = ctxConditionOperator;
                                        break;
                                    }
                            }
                    if (lastLength != -1)
                    {
                        var parts = inputAfterColumnName.Split(' ');
                        var condition = new Condition();
                        condition.Column = tableColumn;
                        condition.ConditionOperator = conditionOperator;
                        condition.Value = parts[lastLength];
                        ParsedInput.Conditions.Add(condition);
                        Logger.Log(
                            "Column: " + condition.Column.ColumnAlias + "\tOperator: " +
                            condition.ConditionOperator.ConditionOperatorName + "\tValue: " + condition.Value,
                            LogManager.Info);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log("An error occured while trying to detect conditions in input, see exception details below",
                    LogManager.Error, ex);
                return false;
            }
        }

        ~QueryParser()
        {
            _ctx.Dispose();
        }
    }
}