﻿using System;
using System.Linq;
using NL2SQLLib.QueryParsing.Model;

namespace NL2SQLLib.QueryParsing.Controller
{
    internal class SelectStatementGenerator : QueryGenerator
    {
        private static readonly LogManager Logger = LogManager.GetLogger();

        public override string GenerateQueryFromParsedInput(ParsedInput parsedInput)
        {
            if (parsedInput.StatementType == null || parsedInput.Table == null)
                throw new Exception("Statement Type and Table Name cannot be null");
            var sqlQuery = "";
            sqlQuery += parsedInput.StatementType.CmdName + " ";
            if (parsedInput.Columns == null || !parsedInput.Columns.Any())
            {
                sqlQuery += "* FROM ";
            }
            else
            {
                for (var i = 0; i < parsedInput.Columns.Count; i++)
                {
                    var column = parsedInput.Columns[i];
                    if (i == parsedInput.Columns.Count - 1)
                        sqlQuery += column.ColumnName + " ";
                    else
                        sqlQuery += column.ColumnName + ", ";
                }
                sqlQuery += "FROM ";
            }
            sqlQuery += parsedInput.Table.TableName;
            if (parsedInput.Conditions != null && parsedInput.Conditions.Any())
            {
                sqlQuery += " WHERE ";
                var count = parsedInput.Conditions.Count;
                foreach (var condition in parsedInput.Conditions)
                {
                    sqlQuery += condition.Column.ColumnName + ' ' + condition.ConditionOperator.ConditionOperatorName;
                    if (condition.Column.DataType.Name.Equals("string", StringComparison.InvariantCultureIgnoreCase))
                        sqlQuery += " '" + condition.Value + "'";
                    else
                        sqlQuery += " " + condition.Value;
                    count--;
                    if (count > 0)
                        sqlQuery += " AND ";
                }
            }
            sqlQuery += ';';
            Logger.Log("Result SQL Query: " + sqlQuery, LogManager.Debug);
            return sqlQuery;
        }
    }
}