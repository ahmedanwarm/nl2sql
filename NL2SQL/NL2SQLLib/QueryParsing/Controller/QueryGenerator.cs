﻿using NL2SQLLib.QueryParsing.Model;

namespace NL2SQLLib.QueryParsing.Controller
{
    internal abstract class QueryGenerator
    {
        public abstract string GenerateQueryFromParsedInput(ParsedInput parsedInput);
    }
}