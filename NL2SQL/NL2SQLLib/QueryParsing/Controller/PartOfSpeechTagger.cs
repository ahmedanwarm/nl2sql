﻿using System.Text;
using OpenNLP.Tools.PosTagger;
using OpenNLP.Tools.SentenceDetect;
using OpenNLP.Tools.Tokenize;

namespace NL2SQLLib.QueryParsing.Controller
{
    internal class PartOfSpeechTagger
    {
        public static string CoordinatingConjunction = "CC";
        public static string CardinalNumber = "CD";
        public static string Determiner = "DT";
        public static string ExistentialThere = "EX";
        public static string ForeignWord = "FW";
        public static string PrepositionSubordinateConjunction = "IN";
        public static string Adjective = "JJ";
        public static string AdjectiveComparative = "JJR";
        public static string AdjectiveSuperlative = "JJS";
        public static string ListItemMarker = "LS";
        public static string Modal = "MD";
        public static string NounSingularOrMass = "NN";
        public static string ProperNounSingular = "NNP";
        public static string ProperNounPlural = "NNPS";
        public static string NounPlural = "NNS";
        public static string Predeterminer = "PDT";
        public static string PossessiveEnding = "POS";
        public static string PersonalPronoun = "PRP";
        public static string PossessivePronoun = "PRP$";
        public static string Adverb = "RB";
        public static string AdverbComparative = "RBR";
        public static string AdverbSuperlative = "RBS";
        public static string Particle = "RP";
        public static string Symbol = "SYM";
        public static string To = "TO";
        public static string Interjection = "UH";
        public static string VerbBaseForm = "VB";
        public static string VerbPastTense = "VBD";
        public static string VerbGerundPresentParticiple = "VBG";
        public static string VerbPastParticiple = "VBN";
        public static string VerbNon3RdPersonSingularPresent = "VBP";
        public static string Verb3RdPersonSingularPresent = "VBZ";
        public static string WhDeterminer = "WDT";
        public static string WhPronoun = "WP";
        public static string PossessiveWhPronoun = "WP$";
        public static string WhAdverb = "WRB";
        public static string LeftOpenDoubleQuote = "``";
        public static string Comma = ",";
        public static string RightCloseDoubleQuote = "''";
        public static string SentenceFinalPunctuation = ".";
        public static string ColonSemiColon = ":";
        public static string DollarSign = "$";
        public static string PoundSign = "#";
        public static string LeftParenthesis = "-LRB-";
        public static string RightParenthesis = "-RRB-";

        private static MaximumEntropySentenceDetector _mSentenceDetector;
        private static EnglishMaximumEntropyTokenizer _mTokenizer;
        private static EnglishMaximumEntropyPosTagger _mPosTagger;
        private static readonly string MModelPath = "C:\\Projects\\DotNet\\OpenNLP\\OpenNLP\\Models\\";

        public static string DetectPos(string input)
        {
            var output = new StringBuilder();

            var sentences = SplitSentences(input);

            foreach (var sentence in sentences)
            {
                var tokens = TokenizeSentence(sentence);
                var tags = PosTagTokens(tokens);

                for (var currentTag = 0; currentTag < tags.Length; currentTag++)
                    output.Append(tokens[currentTag]).Append("/").Append(tags[currentTag]).Append(" ");
            }
            return output.ToString();
        }

        private static string[] SplitSentences(string paragraph)
        {
            if (_mSentenceDetector == null)
                _mSentenceDetector = new EnglishMaximumEntropySentenceDetector(MModelPath + "EnglishSD.nbin");

            return _mSentenceDetector.SentenceDetect(paragraph);
        }

        private static string[] TokenizeSentence(string sentence)
        {
            if (_mTokenizer == null)
                _mTokenizer = new EnglishMaximumEntropyTokenizer(MModelPath + "EnglishTok.nbin");

            return _mTokenizer.Tokenize(sentence);
        }

        private static string[] PosTagTokens(string[] tokens)
        {
            if (_mPosTagger == null)
                _mPosTagger = new EnglishMaximumEntropyPosTagger(MModelPath + "EnglishPOS.nbin",
                    MModelPath + @"\Parser\tagdict");
            return _mPosTagger.Tag(tokens);
        }
    }
}