﻿using NL2SQLLib.EntityFramework.Model;

namespace NL2SQLLib.QueryParsing.Model
{
    internal class Condition
    {
        public UserColumn Column { get; set; }

        public ConditionOperator ConditionOperator { get; set; }

        public string Value { get; set; }
    }
}