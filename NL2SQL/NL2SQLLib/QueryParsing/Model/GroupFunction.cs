﻿using NL2SQLLib.EntityFramework.Model;

namespace NL2SQLLib.QueryParsing.Model
{
    internal class GroupFunction
    {
        public Cmd FunctionName { get; set; }

        public UserColumn ConditionColumn { get; set; }
    }
}