﻿using System.Collections.Generic;
using NL2SQLLib.EntityFramework.Model;

namespace NL2SQLLib.QueryParsing.Model
{
    public class ParsedInput
    {
        public Cmd StatementType { get; set; }

        public UserTable Table { get; set; }

        public List<UserColumn> Columns { get; set; }

        internal List<Condition> Conditions { get; set; }

        internal List<GroupFunction> GroupFunctions { get; set; }

        public UserColumn OrderByColumn { get; set; }

        public bool OrderByOrientation { get; set; }
    }
}