﻿using System;
using System.Collections.Generic;
using NL2SQLLib.EntityFramework.Controller;
using NL2SQLLib.EntityFramework.Model;
using NL2SQLLib.QueryParsing.Controller;

namespace NL2SQLLib.SuggestionsGeneration
{
    public class SuggestionsGenerator
    {
        private static readonly Context Ctx = new Context();
        private readonly QueryParser _parser = new QueryParser();
        private readonly UserSchema _schema;

        public SuggestionsGenerator(int schemaId)
        {
            _schema = Ctx.Schemata.Find(schemaId);
        }

        public List<string> GenerateSuggestionsList(string wholeSentence)
        {
            _parser.RemoveIgnoreWords(wholeSentence);
            var suggestionsList = new List<string>();
            var parsedInput = wholeSentence.Split(' ');
            if (parsedInput.Length == 1)
            {
                var pos1 = PartOfSpeechTagger.DetectPos(parsedInput[0]);
                if (pos1.Contains("V"))
                {
                    CmdSynonym cmdSynonym = null;
                    foreach (var c in Ctx.CommandSynonyms)
                        if (c.Name.Equals(parsedInput[0], StringComparison.InvariantCultureIgnoreCase))
                        {
                            cmdSynonym = c;
                            break;
                        }
                    if (cmdSynonym != null)
                    {
                        var cmdId = cmdSynonym.CmdId;
                        Cmd cmd = null;
                        foreach (var c in Ctx.Commands)
                            if (c.CmdId == cmdId)
                            {
                                cmd = c;
                                break;
                            }
                        if (cmd != null)
                            if (cmd.CmdName == "SELECT" || cmd.CmdName == "UPDATE")
                            {
                                var ds = new Dictionary<int, string>();
                                suggestionsList.Clear();
                                foreach (var c in _schema.Tables)
                                {
                                    suggestionsList.Add(c.TableAlias);
                                    ds[c.UserTableId] = c.TableAlias;
                                }
                                foreach (var d in ds)
                                foreach (var k in Ctx.Columns)
                                    if (d.Key == k.TableId)
                                        suggestionsList.Add(d.Value + " " + k.ColumnAlias);
                            }
                            else if (cmd.CmdName == "DELETE")
                            {
                                suggestionsList.Clear();
                                foreach (var c in _schema.Tables)
                                    suggestionsList.Add(c.TableAlias);
                            }
                    }
                }
            }
            return suggestionsList;
        }
    }
}