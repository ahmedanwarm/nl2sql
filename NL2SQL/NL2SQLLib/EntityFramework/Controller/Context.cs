﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using NL2SQLLib.EntityFramework.Model;

namespace NL2SQLLib.EntityFramework.Controller
{
    public class Context : DbContext
    {
        public Context() : base("name=NL2SQLDBConnectionString")
        {
        }

        public DbSet<Cmd> Commands { get; set; }
        public DbSet<CmdSynonym> CommandSynonyms { get; set; }
        public DbSet<UserSchema> Schemata { get; set; }
        public DbSet<UserTable> Tables { get; set; }
        public DbSet<UserColumn> Columns { get; set; }
        public DbSet<DataType> DataTypes { get; set; }
        public DbSet<ConditionOperator> ConditionOperators { get; set; }
        public DbSet<ConditionOperatorAlias> ConditionOperatorAliases { get; set; }

        public UserSchema GetSchemaById(int schemaId)
        {
            UserSchema selectedSchema = null;
            foreach (var schema in Schemata)
                if (schema.UserSchemaId == schemaId)
                {
                    selectedSchema = schema;
                    break;
                }
            return selectedSchema;
        }

        public List<CmdSynonym> GetSynonymsByCmdName(string cmdName)
        {
            var cmdSynonyms = new List<CmdSynonym>();
            Cmd targetCmd = null;
            foreach (var cmd in Commands)
                if (cmd.CmdName.Equals(cmdName, StringComparison.InvariantCultureIgnoreCase))
                {
                    targetCmd = cmd;
                    break;
                }
            if (targetCmd != null)
                foreach (var cmdSynonym in targetCmd.CmdSynonyms)
                    cmdSynonyms.Add(cmdSynonym);
            return cmdSynonyms;
        }
    }
}