﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NL2SQLLib.EntityFramework.Model
{
    public class CmdSynonym
    {
        public int CmdSynonymId { get; set; }
        public string Name { get; set; }
        public int CmdId { get; set; }

        [ForeignKey("CmdId")]
        public virtual Cmd Cmd { get; set; }
    }
}