﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NL2SQLLib.EntityFramework.Model
{
    public class UserColumn
    {
        public string ParentTableName;
        public int UserColumnId { get; set; }
        public string ColumnName { get; set; }
        public string ColumnAlias { get; set; }
        public bool IsPk { get; set; }
        public bool IsFk { get; set; }
        public string FkTarget { get; set; }
        public int TableId { get; set; }
        public int DataTypeId { get; set; }

        [ForeignKey("TableId")]
        public virtual UserTable ParentTable { get; set; }

        [ForeignKey("DataTypeId")]
        public virtual DataType DataType { get; set; }
    }
}