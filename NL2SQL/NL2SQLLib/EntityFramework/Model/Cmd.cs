﻿using System.Collections.Generic;

namespace NL2SQLLib.EntityFramework.Model
{
    public class Cmd
    {
        public int CmdId { get; set; }
        public string CmdName { get; set; }
        public virtual ICollection<CmdSynonym> CmdSynonyms { get; set; }
    }
}