﻿using System.Collections.Generic;

namespace NL2SQLLib.EntityFramework.Model
{
    public class DataType
    {
        public int DataTypeId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<UserColumn> Columns { get; set; }
        public virtual ICollection<ConditionOperator> ConditionOperators { get; set; }
    }
}