﻿using System.Collections.Generic;

namespace NL2SQLLib.EntityFramework.Model
{
    public class UserSchema
    {
        public UserSchema()
        {
            Tables = new List<UserTable>();
        }

        public int UserSchemaId { get; set; }
        public string SchemaName { get; set; }
        public string SchemaAlias { get; set; }
        public virtual ICollection<UserTable> Tables { get; set; }
    }
}