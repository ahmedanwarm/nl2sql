﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NL2SQLLib.EntityFramework.Model
{
    public class ConditionOperatorAlias
    {
        public int ConditionOperatorAliasId { get; set; }
        public string ConditionOperatorAliasName { get; set; }
        public int ParentConditionOperatorId { get; set; }

        [ForeignKey("ParentConditionOperatorId")]
        public virtual ConditionOperator ParentConditionOperator { get; set; }
    }
}