﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NL2SQLLib.EntityFramework.Model
{
    public class UserTable
    {
        public UserTable()
        {
            Columns = new List<UserColumn>();
        }

        public int UserTableId { get; set; }
        public string TableName { get; set; }
        public string TableAlias { get; set; }
        public int SchemaId { get; set; }

        [ForeignKey("SchemaId")]
        public UserSchema Schema { get; set; }

        public virtual ICollection<UserColumn> Columns { get; set; }
    }
}