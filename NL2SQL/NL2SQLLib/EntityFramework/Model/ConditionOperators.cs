﻿using System.Collections.Generic;

namespace NL2SQLLib.EntityFramework.Model
{
    public class ConditionOperator
    {
        public ConditionOperator()
        {
            ConditionOperatorAliases = new List<ConditionOperatorAlias>();
        }

        public int ConditionOperatorId { get; set; }
        public string ConditionOperatorName { get; set; }
        public virtual ICollection<ConditionOperatorAlias> ConditionOperatorAliases { get; set; }
        public ICollection<DataType> AllowedDataTypes { get; set; }
    }
}