﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using NL2SQLLib.EntityFramework.Controller;
using NL2SQLLib.EntityFramework.Model;

namespace NL2SQLLib.SchemaParsing
{
    public class SchemaParser
    {
        private static readonly XmlDocument XmlDoc = new XmlDocument();
        private static UserSchema _schema = new UserSchema();
        private static readonly LogManager Logger = LogManager.GetLogger();
        private static readonly Context Ctx = new Context();
        private static int _validationErrorsCount;

        private SchemaParser()
        {
        }

        public static bool ValidateXmlFile(string xmlFilePath)
        {
            try
            {
                _validationErrorsCount = 0;
                var schemaSet = new XmlSchemaSet();
                schemaSet.Add("urn:nl2sql", "E:\\schema.xsd");
                return Validate("E:\\schema-template.xml", schemaSet);
            }
            catch (Exception ex)
            {
                Logger.Log("Error reading schema XML file", LogManager.Error, ex);
                return false;
            }
        }

        private static bool Validate(string filename, XmlSchemaSet schemaSet)
        {
            Console.WriteLine();
            Console.WriteLine("\r\nValidating XML file {0}...", filename);

            XmlSchema compiledSchema = null;

            foreach (XmlSchema schema in schemaSet.Schemas())
                compiledSchema = schema;

            var settings = new XmlReaderSettings();
            settings.Schemas.Add(compiledSchema);
            settings.ValidationEventHandler += ValidationCallBack;
            settings.ValidationType = ValidationType.Schema;

            //Create the schema validating reader.
            var vreader = XmlReader.Create(filename, settings);

            while (vreader.Read())
            {
            }

            //Close the reader.
            vreader.Close();
            return _validationErrorsCount == 0;
        }

        //Display any warnings or errors.
        private static void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
            {
                Logger.Log("\tWarning: Matching schema not found.  No validation occurred." + args.Message,
                    LogManager.Warning);
                _validationErrorsCount++;
            }
            else
            {
                Logger.Log("\tValidation error: " + args.Message, LogManager.Error);
                _validationErrorsCount++;
            }
        }

        public static void ReadXmlFile(string xmlFilePath)
        {
            var dataTypes = Ctx.DataTypes.ToList();
            try
            {
                _schema = new UserSchema();
                XmlDoc.Load(xmlFilePath);
                var schemaXml = XmlDoc.ChildNodes[0];
                for (var i = 0; i < schemaXml.ChildNodes.Count; i++)
                {
                    var currentTable = new UserTable();
                    if (schemaXml.Attributes != null)
                    {
                        _schema.SchemaName = schemaXml.Attributes["name"].Value;
                        _schema.SchemaAlias = schemaXml.Attributes["alias"].Value;
                    }
                    var tableXml = schemaXml.ChildNodes[i];
                    if (tableXml.Attributes != null)
                    {
                        currentTable.TableName = tableXml.Attributes["ID"].Value;
                        currentTable.TableAlias = tableXml.Attributes["alias"].Value;
                        for (var j = 0; j < tableXml.ChildNodes.Count; j++)
                        {
                            var currentColumn = new UserColumn();
                            var columnXml = tableXml.ChildNodes[j];
                            if (columnXml.Attributes == null) continue;
                            currentColumn.ColumnName = columnXml.Attributes["ID"].Value;
                            currentColumn.ColumnAlias = columnXml.Attributes["alias"].Value;
                            try
                            {
                                var dataTypeStr = columnXml.Attributes["DataType"].Value;
                                var dataType = dataTypes.Find(x => x.Name == dataTypeStr);
                                if (dataType == null)
                                {
                                    dataType = new DataType {Name = columnXml.Attributes["DataType"].Value};
                                    dataTypes.Add(dataType);
                                }
                                currentColumn.DataType = dataType;
                            }
                            catch (Exception ex)
                            {
                                Logger.Log(
                                    "Something went wrong trying to get Data Type from database, check details below",
                                    LogManager.Error, ex);
                                currentColumn.DataType = new DataType {Name = columnXml.Attributes["DataType"].Value};
                            }

                            if (columnXml.Attributes["PK"].Value == "true")
                                currentColumn.IsPk = true;
                            else
                                currentColumn.IsPk = false;

                            if (columnXml.Attributes["FK"].Value == "true")
                            {
                                currentColumn.IsFk = true;
                                currentColumn.FkTarget = columnXml.Attributes["FKTable"].Value + "." +
                                                         columnXml.Attributes["FKColumn"].Value;
                            }
                            else
                            {
                                currentColumn.IsFk = false;
                            }
                            currentColumn.ParentTableName = columnXml.Attributes["ParentTable"].Value;
                            currentTable.Columns.Add(currentColumn);
                        }
                    }
                    _schema.Tables.Add(currentTable);
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error reading schema XML file", LogManager.Error, ex);
            }
        }

        public static void DescribeSchema()
        {
            if (_schema.Tables.Count == 0)
            {
                Console.WriteLine("No schema file read");
                return;
            }
            var schemaDescription = "\nSchema Name: " + _schema.SchemaName + "\nSchema Alias: " + _schema.SchemaAlias +
                                    "\n";
            for (var i = 0; i < _schema.Tables.Count; i++)
                schemaDescription += DescribeTable(_schema.Tables.ElementAt(i)) + "\n";
            Logger.Log(schemaDescription, LogManager.Debug);
        }

        public static string DescribeTable(UserTable table)
        {
            var description = "\n";
            description += "Name: " + table.TableName + "\n";
            description += "Alias: " + table.TableAlias + "\n";
            description += "Number of Columns: " + table.Columns.Count + "\nColumns:\n";
            for (var i = 0; i < table.Columns.Count; i++)
            {
                description += "\t\tColumn #" + i + "\n";
                description += "\t\t\tName: " + table.Columns.ElementAt(i).ColumnName + "\n";
                description += "\t\t\tAlias: " + table.Columns.ElementAt(i).ColumnAlias + "\n";
                description += "\t\t\tData Type: " + table.Columns.ElementAt(i).DataType.Name + "\n";
                if (table.Columns.ElementAt(i).IsPk)
                    description += "\t\t\tPrimary Key: true\n";
                else
                    description += "\t\t\tPrimary Key: false\n";
                if (table.Columns.ElementAt(i).IsFk)
                {
                    description += "\t\t\tForeign Key: true\n";
                    description += "\t\t\tForeign Key Target: " + table.Columns.ElementAt(i).FkTarget + "\n";
                }
                else
                {
                    description += "\t\t\tForeign Key: false\n";
                }
            }
            return description;
        }

        public static UserSchema SaveSchemaToDb()
        {
            try
            {
                Ctx.Schemata.Add(_schema);
                Ctx.SaveChanges();
                Logger.Log("Saved schema details successfully", LogManager.Info);
            }
            catch (Exception ex)
            {
                Logger.Log("Something went wrong saving schema details to the database", LogManager.Error, ex);
            }
            return _schema;
        }

        public static int AddSchemaDetailsToAutocorrectFile(string filePath)
        {
            int result;
            try
            {
                var fs = new FileStream(filePath, FileMode.Open);
                var sr = new StreamReader(fs);
                var schemaDetails = "\r\n";
                var file = sr.ReadToEnd().ToLower();
                sr.Close();
                fs.Close();
                foreach (var table in _schema.Tables)
                {
                    if (!file.Contains("\r\n" + table.TableAlias.ToLower() + "\r\n") &&
                        !schemaDetails.Contains("\r\n" + table.TableAlias.ToLower() + "\r\n"))
                        schemaDetails += table.TableAlias.ToLower() + '\r' + '\n';
                    var temp = GetPlural(table.TableAlias);
                    if (!file.Contains("\r\n" + temp) &&
                        !schemaDetails.Contains("\r\n" + temp))
                        schemaDetails += temp;

                    if (!file.Contains("\r\n" + table.TableName.ToLower() + "\r\n") &&
                        !schemaDetails.Contains("\r\n" + table.TableName.ToLower() + "\r\n"))
                        schemaDetails += table.TableName.ToLower() + '\r' + '\n';
                    temp = GetPlural(table.TableName);
                    if (!file.Contains("\r\n" + temp) &&
                        !schemaDetails.Contains("\r\n" + temp))
                        schemaDetails += temp;
                    foreach (var column in table.Columns)
                    {
                        if (!file.Contains("\r\n" + column.ColumnAlias.ToLower() + "\r\n") &&
                            !schemaDetails.Contains("\r\n" + column.ColumnAlias.ToLower() + "\r\n"))
                            schemaDetails += column.ColumnAlias.ToLower() + '\r' + '\n';
                        temp = GetPlural(column.ColumnAlias);
                        if (!file.Contains("\r\n" + temp) &&
                            !schemaDetails.Contains("\r\n" + temp))
                            schemaDetails += temp;

                        if (!file.Contains("\r\n" + column.ColumnName.ToLower() + "\r\n") &&
                            !schemaDetails.Contains("\r\n" + column.ColumnName.ToLower() + "\r\n"))
                            schemaDetails += column.ColumnName.ToLower() + '\r' + '\n';
                        temp = GetPlural(column.ColumnName);
                        if (!file.Contains("\r\n" + temp) &&
                            !schemaDetails.Contains("\r\n" + temp))
                            schemaDetails += temp;
                    }
                }
                fs = new FileStream(filePath, FileMode.Append);
                var sw = new StreamWriter(fs);
                if (!schemaDetails.Equals("\r\n"))
                    sw.Write(schemaDetails);
                sw.Close();
                fs.Close();
                result = 1;
            }
            catch (Exception ex)
            {
                Logger.Log("Exception details: ", LogManager.Error, ex);
                result = 0;
            }
            return result;
        }

        public static string GetPlural(string singular)
        {
            singular = singular.ToLower();
            if (singular.EndsWith("ay") ||
                singular.EndsWith("ey") ||
                singular.EndsWith("iy") ||
                singular.EndsWith("oy") ||
                singular.EndsWith("uy"))
                return singular.ToLower().TrimEnd('y') + "s\r\n";
            if (singular.EndsWith("y"))
                return singular.ToLower().TrimEnd('y') + "ies\r\n";
            if (singular.EndsWith("s") ||
                singular.EndsWith("ss") ||
                singular.EndsWith("sh") ||
                singular.EndsWith("ch") ||
                singular.EndsWith("x") ||
                singular.EndsWith("z"))
                return singular.ToLower() + "es\r\n";
            if (singular.EndsWith("f") ||
                singular.EndsWith("fe"))
                return singular.ToLower().TrimEnd('e').TrimEnd('f') + "ves\r\n";
            if (singular.EndsWith("o"))
                return singular.ToLower() + "es\r\n";
            if (singular.EndsWith("us"))
                return singular.ToLower().TrimEnd('s').TrimEnd('u') + "i\r\n";
            if (singular.EndsWith("is"))
                return singular.ToLower().TrimEnd('s').TrimEnd('i') + "es\r\n";
            if (singular.EndsWith("on"))
                return singular.ToLower().TrimEnd('n').TrimEnd('o') + "a\r\n";
            return singular.ToLower() + "s\r\n";
        }

        ~SchemaParser()
        {
            Ctx.Dispose();
        }
    }
}