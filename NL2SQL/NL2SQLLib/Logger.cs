﻿using System;
using log4net;
using log4net.Config;

namespace NL2SQLLib
{
    public class LogManager
    {
        public const int Debug = 1;
        public const int Info = 2;
        public const int Warning = 3;
        public const int Error = 4;

        private static LogManager _instance;
        private static ILog _logger;

        private static readonly object Padlock = new object();

        static LogManager()
        {
        }

        private LogManager()
        {
            _logger = log4net.LogManager.GetLogger("NL2SQL");
            XmlConfigurator.Configure();
        }

        public static LogManager GetLogger()
        {
            lock (Padlock)
            {
                if (_instance == null || _logger == null)
                    _instance = new LogManager();
                return _instance;
            }
        }

        public void Log(string message, int level)
        {
            switch (level)
            {
                case Debug:
                    _logger.Debug(message);
                    Console.WriteLine(DateTime.Now + " - DEBUG: " + message);
                    break;
                case Info:
                    _logger.Info(message);
                    Console.WriteLine(DateTime.Now + " - INFO: " + message);
                    break;
                case Warning:
                    _logger.Warn(message);
                    Console.WriteLine(DateTime.Now + " - WARNING: " + message);
                    break;
                case Error:
                    _logger.Error(message);
                    Console.WriteLine(DateTime.Now + " - ERROR: " + message);
                    break;
                default:
                    _logger.Error("Unknown log level");
                    _logger.Info(message);
                    Console.WriteLine(DateTime.Now + " - UNKNOWN: " + "\nUnknown log level\n" + message);
                    break;
            }
        }

        public void Log(string message, int level, object exception)
        {
            var e = (Exception) exception;
            switch (level)
            {
                case Warning:
                    _logger.Warn(message, (Exception) exception);
                    Console.WriteLine(DateTime.Now + " - WARNING: " + message + "\nException Details:\n" + e.Source +
                                      e.Message + e.StackTrace);
                    break;
                case Error:
                    _logger.Error(message, (Exception) exception);
                    Console.WriteLine(DateTime.Now + " - ERROR: " + message + "\nException Details:\n" + e.Source +
                                      e.Message + e.StackTrace);
                    break;
                default:
                    _logger.Error("Unknown log level");
                    _logger.Info(message);
                    Console.WriteLine(DateTime.Now + " - UNKNOWN: " + message + "\nException Details:\n" + e.Source +
                                      e.Message + e.StackTrace);
                    break;
            }
        }
    }
}